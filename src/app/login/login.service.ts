import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';



/**
 * @description
 * @class
 */
@Injectable()
export class LoginService {
  
  private url = 'http://13.59.192.179:5000/api/register/login';
  constructor(private Http: HttpClient) {
    
  }
  loginUser(email: string,password: string){
    return this.Http.post( this.url, {'email': email,'password': password } );
  }

}
