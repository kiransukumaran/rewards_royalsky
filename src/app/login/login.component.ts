import { HeaderService } from './../header/header.service';
import { Component, OnInit, ElementRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(private ls: LoginService, private router: Router, private hS: HeaderService) { }

  ngOnInit() {
  }
  login() {
    this.ls.loginUser(this.email, this.password).subscribe(res => {

      if (res['status'] == 200) {

        sessionStorage.setItem(this.setuserid(), res["data"]["value"]["userid"]);
        this.hS.setisLoggedinFlagtrue();
        this.router.navigate(['/profile']);


      } else {
        alert("Invalid Username or Password");
      }

    }

    );
  }

  register() {
    this.router.navigate(['/register']);
  }

  private setloggedin(): string {
    return "royalrewardloggedin";
  }

  private setuserid(): string {
    return "royalrewarduserid";
  }

  showPass(pwd) {
    if (pwd.type === "password") {
      pwd.type = "text";
    } else {
      pwd.type = "password";
    }
  }

}


