import { MembershipcardService } from './membershipcard/membershipcard.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { BodyComponent } from './body/body.component';
import { LoginComponent } from './login/login.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { LoginService } from './login/login.service';
import { ProfileComponent } from './profile/profile.component';
import { RouterModule } from '@angular/router';
import { HeaderService } from './header/header.service';
import { ProfileService } from './profile/profile.service';
import { RewardtableService } from './reward-table/rewardtable.service';
import { RewardTableComponent } from './reward-table/rewardtable.component';
import { MembershipcardComponent } from './membershipcard/membershipcard.component';
import { CardnumberPipe } from './membershipcard/cardnumber.pipe';
import { LoginAuth } from './app.loginauth';
import { RegisterComponent } from './register/register.component';
import { RegisterService } from './register/register.service';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    BodyComponent,
    LoginComponent,
    ProfileComponent,
    RewardTableComponent,
    MembershipcardComponent,
    RegisterComponent,
    CardnumberPipe,
    TermsComponent,
    PrivacyComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [LoginService, HeaderService, ProfileService, RewardtableService, MembershipcardService, LoginAuth, RegisterService],
  bootstrap: [AppComponent]
})
export class AppModule { }
