import { Component, OnInit } from "@angular/core";
import { RegisterService } from './register.service';
import { Router } from '@angular/router';
import { FormsModule } from '@angular/forms';

// import { DatepickerModule } from 'ngx-date-picker';

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"]
})


export class RegisterComponent implements OnInit {
  
  data : any = [];
  registerDetails : any;
  firstname: String
  lastname: String
  email: String
  password: String
  dob: String
  contactno: Number
  whatsappno: Number
  address: String
  city: String
  state: String
  country: String
  zip: Number
  relationshipstatus: String
  weddingdate: String
  nameofspouse: String
  membershiptype: Number
  travelStatus: String
  travelLocn: String

  constructor(private rs: RegisterService, private router: Router) { 

  }

  relationstatus: any;

  ngOnInit() {

  }
  test() {
  this.relationstatus = "Married"
  }
  test1() {
    this.relationstatus = "Unmarried"
  }

  register()  {
    this.registerDetails = {
      firstname : this.firstname,
      lastname: this.lastname,
      email: this.email,
      password: this.password,
      dob: this.dob,
      contactno: this.contactno,
      whatsappno: this.whatsappno,
      address: this.address,
      city: this.city,
      state: this.state,
      country: this.country,
      zip: this.zip,
      relationshipstatus: this.relationshipstatus,
      weddingdate: this.weddingdate,
      nameofspouse: this.nameofspouse,
      membershiptype: 1,
      travelLocn: this.travelLocn,
      travelStatus: this.travelStatus
    }
    this.rs.registerUser(this.registerDetails).subscribe(res => { 
      if( res['auth'] == true ){
        this.router.navigate(['/login']);
      }
    } 
    );
  }

}