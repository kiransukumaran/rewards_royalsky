import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';` `

/**
 * @description
 * @class
 */
@Injectable()
export class RegisterService {

  private url = 'http://13.59.192.179:5000/api/register/';
  private newurl = 'http://13.59.192.179:5000/api/membership/';
  constructor(private Http: HttpClient) {
    
  }
  registerUser(registerDetails: any){
    return this.Http.post( this.url, registerDetails );
  }
}

