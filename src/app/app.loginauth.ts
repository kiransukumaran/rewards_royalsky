import { ProfileService } from './profile/profile.service';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

export class LoginAuth implements CanActivate {

    constructor(private profileService: ProfileService, private router: Router) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        if (this.profileService.isValidUser())
            return true;
        else {
            this.router.navigate(['/login']);
            return false;
        }

    }
}