import { Component, OnInit } from '@angular/core';
import { RewardtableService } from './rewardtable.service';
@Component({
  selector: 'app-reward-table',
  templateUrl: './rewardtable.component.html',
  styleUrls: ['./rewardtable.component.css']
})
export class RewardTableComponent implements OnInit {

  data: any;
  index: number;
  length: number;

  constructor(private rewardTable: RewardtableService) {}

  showRewardTable() {
    this.rewardTable.getRewardHistory().subscribe(res => {
      this.data = res;
      this.length = this.data.length
      console.log(this.data)
    });
  }

  ngOnInit() {
    this.showRewardTable();
  }
  
}
