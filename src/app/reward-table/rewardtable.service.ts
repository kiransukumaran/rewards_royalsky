import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class RewardtableService {

  userid: string;

  pointUrl: string;
  constructor(private Http: HttpClient) {
    this.userid = sessionStorage.getItem("royalrewarduserid");
    this.pointUrl = 'http://13.59.192.179:5000/api/point/' + this.userid;
  }
  getRewardHistory() {
    return this.Http.get(this.pointUrl)

  }
}
