import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cardnumber'
})
export class CardnumberPipe implements PipeTransform {

  transform(value: string, limit: number): any {
    value = value.toString();
    let length = value.length;
    let i = 0;
    let result: string = "";
    while (i <= length) {
      result = result + " " + value.substring(i, i + limit);
      i = i + limit;
    }
    return result;
  }

}
