import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { HeaderService } from '../header/header.service';
import { MembershipcardService } from './membershipcard.service';

@Component({
  selector: 'app-membershipcard',
  templateUrl: './membershipcard.component.html',
  styleUrls: ['./membershipcard.component.css']
})
export class MembershipcardComponent implements OnInit {
  //userid : string;
  memberStatus: string;
  name: string;
  card: string;
  cardno: string = "XXX XXXX XXXX";
  username: string = "User Name";
  cardvalidfrom: string;
  cardvalidtill: string;
  data:any;
  cardSecret: string;

  constructor(private hS: HeaderService, private mS: MembershipcardService, private router: Router) {

  }

  showMemberCard() {
    this.mS.cardUser().subscribe(res => {
      this.data = res;
      this.cardno = this.data.membership.cardno
      this.username = sessionStorage.getItem("royalrewardusername").toUpperCase();
      this.cardvalidfrom = this.data.membership.issuedate;
      this.cardvalidtill = this.data.membership.validto;
      this.cardSecret = this.data.membership._id;
    });

  }




  ngOnInit() {
    this.card = '../../assets/images/membership_lb_card.png';
    this.showMemberCard();



    this.hS.hideGetStartedFlag();

  }
}
