import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MembershipcardService {
  userid: string;
  url: string;
  pointUrl: string;
  constructor(private Http: HttpClient) {
    this.userid = sessionStorage.getItem("royalrewarduserid");
    this.url = 'http://13.59.192.179:5000/api/register/' + this.userid;
  }
  cardUser() {
    return this.Http.get(this.url);

  }

}
