import { Injectable } from "@angular/core";

/**
 * @description
 * @class
 */
@Injectable()
export class HeaderService {

  getStartedFlag: any = true;
  tempstring: string = "false";


  constructor() {

  }
  private gettokenloginid(): string {
    return "royalrewardloggedin";
  }

  getisLoggedinFlag() {
    this.tempstring = sessionStorage.getItem(this.gettokenloginid());
    if (!(this.tempstring == null)) {
      return ((this.tempstring == 'true') ? true : false);
    }
    else {
      return false;
    }
  }
  //this.hS.setisLoggedinFlagtrue();
  setisLoggedinFlagtrue() {
    sessionStorage.setItem(this.gettokenloginid(), 'true');
  }
  setisLoggedinFlagfalse() {
    sessionStorage.setItem(this.gettokenloginid(), 'false');
  }

  removeisLoggedinFlag() {
    sessionStorage.removeItem(this.gettokenloginid());
  }




  showGetStartedFlag() {

    return this.getStartedFlag = true;


  }

  hideGetStartedFlag() {
    return this.getStartedFlag = false;
  }


}
