import { Component, OnInit, Input, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { HeaderService } from './header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  flag: any;


  constructor(private router: Router, public hS: HeaderService) {



  }
  userHome() {
    this.hS.showGetStartedFlag();
    // sessionStorage.setItem(this.gettokenloginid(), 'true');
    //this.hS.setisLoggedinFlagtrue();
    // sessionStorage.clear();
  }


  hidenavbar() {

    let a = document.querySelector("#navbarsExampleDefault");
    a.classList.remove("show");


  }


  ngOnInit() {

    this.flag = this.hS.getStartedFlag;


  }

}
