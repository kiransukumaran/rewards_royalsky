import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { BodyComponent } from './body/body.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginAuth } from './app.loginauth';
import { RegisterComponent } from './register/register.component';
import { TermsComponent } from './terms/terms.component';
import { PrivacyComponent } from './privacy/privacy.component';

const routes: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "profile",
    component: ProfileComponent,
    canActivate: [LoginAuth],
  },
  {
    path: "register",
    component: RegisterComponent
  },
  {
    path: "terms",
    component: TermsComponent
  },
  {
    path: "privacy",
    component: PrivacyComponent
  },
  {
    path: "",
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: "**",
    component: LoginComponent,

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
