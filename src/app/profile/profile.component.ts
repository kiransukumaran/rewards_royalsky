import { Component, OnInit } from "@angular/core";
import { HeaderService } from '../header/header.service';
import { ProfileService } from './profile.service';
import { Router } from '@angular/router';
@Component({
  selector: "profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.css"]
})

export class ProfileComponent implements OnInit {
  //userid : string;
  memberStatus: string;
  name: string;
  toggle: boolean;
  settings: boolean;
  smallDevice: boolean;
  _card: boolean;
  _table: boolean;
  _accounts: boolean;
  _data: any;
  _totalpoints = 0;





  constructor(private hS: HeaderService, private pS: ProfileService, private router: Router) {
    // document.querySelector("#rewardPoints").innerHTML = "Loyalty Reward Points";
    this._card = false;
    this._table = false;
    this._accounts = false;
  }
  private gettokenloginid(): string {
    return "royalrewardloggedin";
  }
  private gettokenuserid(): string {
    return "royalrewarduserid";
  }

  userHome() {
    this.hS.showGetStartedFlag();
    // sessionStorage.setItem(this.gettokenloginid(), 'true');
    this.hS.setisLoggedinFlagtrue();
    // sessionStorage.clear();
    this.router.navigate(['']);


  }

  userLogout() {
    // this.hS.removeisLoggedinFlag();
    sessionStorage.removeItem("royalrewarduserid");
    sessionStorage.removeItem("royalrewardusername");
    sessionStorage.removeItem("royalrewardloggedin");
    sessionStorage.removeItem("royalrewarduseremailid");

    this.hS.showGetStartedFlag();
    sessionStorage.clear();
    this.router.navigate(['/login']);


  }

  //Mobile view
  showCard() {

    this._card = true;
    this._table = false;
    this._accounts = false;
  }
  showAccruals() {
    this._card = false;
    this._table = true;
    this._accounts = false;

  }
  showUserAccount() {
    this._card = false;
    this._table = false;
    this._accounts = true;
  }



  //Lap view
  sidebarData() {
    this.pS.profileUser().subscribe(res => {

      this.name = res["firstname"] + " " + res["lastname"];
      switch (res["membershiptype"]) {
        case 1: {
          this.memberStatus = "Member";
          break;
        }
        case 2: {
          this.memberStatus = "Plus Member";
          break;
        }
        case 3: {
          this.memberStatus = "Freelance Member";
          break;
        }
        default: {
          this.memberStatus = "Member";
          break;
        }
      }

      document.querySelector("#profileUserName").innerHTML = this.name;
      document.querySelector("#profileUserEmail").innerHTML = res["email"];
      document.querySelector("#profileMembershipStatus").innerHTML = this.memberStatus;
      sessionStorage.setItem(this.settokenusername(), this.name);
      sessionStorage.setItem(this.settokenuseremail(), res["email"]);
    });
  }




  private settokenusername(): string {
    return "royalrewardusername";
  }
  private settokenuseremail(): string {
    return "royalrewarduseremailid";
  }

  toggleComponents() {
    this.toggle = !this.toggle;

  }
  userPointData() {
    this.pS.userPoint().subscribe(res => {

      this._data = res;
      this._data.forEach(element => {
        this._totalpoints += element.last_added;
      });

      document.querySelector("#rewardPoints").innerHTML = "Loyalty Reward Points: " + this._totalpoints;
    });

  }

  ngOnInit() {
    if( sessionStorage.getItem( 'royalrewardloggedin' ) == 'true' && sessionStorage.getItem( 'royalrewarduserid' ) ){
      let scope = this;
    if (window.innerWidth < 513) {
      scope.smallDevice = true;
    }
    else {
      scope.smallDevice = false;
    }
    window.addEventListener('resize', function (event) {
      if (window.innerWidth < 513) {
        scope.smallDevice = true;
      }
      else {
        scope.smallDevice = false;
      }
    });

    this.toggle = true;
    this.settings = false;
    this.sidebarData();
    this.userPointData();
    this.hS.hideGetStartedFlag();

    } else {
      this.router.navigate(['login'])
    }
  }

  

}