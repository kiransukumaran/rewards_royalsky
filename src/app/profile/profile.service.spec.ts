import { ProfileService } from "./profile.service";
import { TestBed } from "@angular/core/testing";

describe("ProfileService", () => {

  let service: ProfileService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ProfileService
      ]
    });
    service = TestBed.get(ProfileService);

  });

  it("should be able to create service instance", () => {
    expect(service).toBeDefined();
  });

});
