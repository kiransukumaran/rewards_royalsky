import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

/**
 * @description
 * @class
 */
@Injectable()
export class ProfileService {
  userid: string;
  url: string;
  pointUrl: string;
  constructor(private Http: HttpClient) {
    this.userid = sessionStorage.getItem("royalrewarduserid");
    this.url = 'http://13.59.192.179:5000/api/register/' + this.userid;
    this.pointUrl = 'http://13.59.192.179:5000/api/point/' + this.userid;
  }
  profileUser() {
    return this.Http.get(this.url);

  }
  userPoint() {
    return this.Http.get(this.pointUrl);
  }

  isValidUser() {
    if (this.userid) {
      return true;
    }
    return false;
  }

}
